import styled from 'styled-components'

const Tile = styled.div`
    width: 100px;
    height: 100px;
    border: 1px dashed black;
    box-sizing: border-box;
`;

const Robot = ({src, facing}) => {

    let deg = 0;
    if (facing === 'WEST') deg = 90
    else if (facing === 'NORTH') deg = 180
    else if (facing === 'EAST') deg = 270

    return <img src={src} style={{
            width: "100px",
            height: "100px",
            transform: `rotate(${deg}deg)`
        }}/>
}
const Grid = ({size, facing, position}) => {
    let tiles = [];
    for (let y = size - 1; y >= 0; y--) {
        for (let x = 0; x < size; x++) {
            console.log(facing);
            tiles.push(
                <Tile key={`${x},${y}`}>
                    {
                        (
                            position && 
                            facing &&
                            x == position.x &&
                            y == position.y 
                        ) ? <Robot src='/static/robot.svg' facing={facing} />
                            : null
                    }
                </Tile>
            )
        }
    }

    return (
        <div style={{
            display: "grid",
            gridTemplateRows: `repeat(${size}, 1fr)`,
            gridTemplateColumns: `repeat(${size}, 1fr)`,
            margin: "2rem 0"
        }}>
            {
                tiles
            }
        </div>
    )
}

export default Grid;