import styled from 'styled-components'
import * as types from '../redux/types'
import { connect } from 'react-redux'

const I = styled.input`
	box-sizing: border-box;
	width: 100%;
`;
const Input = ({dispatch}) => {

	return <I onKeyPress={
			e => {
				if (e.key === 'Enter') {
					let cmd = e.target.value;
					try {
						let rgx = /PLACE (\d),(\d) (\w+)/;

						let m = rgx.exec(cmd);
						let x = m[1];
						let y = m[2];
						let facing = m[3];
						dispatch({type: types.PLACE, facing, position: {x,y}});
					} catch(e) {}

					switch (cmd) {
						case types.MOVE:
							dispatch({type: types.MOVE})
							break;
						case types.LEFT:
							dispatch({type: types.LEFT})
							break;
						case types.RIGHT:
							dispatch({type: types.RIGHT})

					}
					e.target.value = ""
				}
			}
		} type='text'/>
}

export default connect()(Input)