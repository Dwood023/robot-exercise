import React from 'react';
import { Provider } from 'react-redux'
import withReduxStore from '../redux/with-redux-store'

import '../styles/global.sass';

const MyApp = ({Component, pageProps, reduxStore}) => (
    <Provider store={reduxStore}>
        <Component {...pageProps} />
    </Provider>
)

export default withReduxStore(MyApp);
