import styled from 'styled-components'
import { connect } from 'react-redux'
import { BOARD_SIZE } from '../redux/types'
import Input from '../components/input'
import Grid from '../components/grid'

const Col = styled.div`
    margin: auto;
    width: 500px;
`;

const Board = ({position, facing}) => {
    console.log("position" + position);

return <Col>
    <Grid size={BOARD_SIZE.x} facing={facing} position={position} />

    <Input/>
    {
        position && facing ? <Report position={position} facing={facing} />
        : null
    }
    {attrib}
</Col>
}

const Report = ({position ,facing}) => <>
    <span>{position.x},{position.y}</span>
    <div>{facing}</div>
</>
const attrib = <div style={{fontSize: '8pt', position: 'absolute', bottom: '0px'}}>
    Robot icon stolen from <a href="https://www.flaticon.com/authors/simpleicon" title="SimpleIcon">SimpleIcon</a> from <a href="https://www.flaticon.com/"             title="Flaticon">www.flaticon.com</a>
</div>

const mapStateToProps = (state) => ({
    position: state.position,
    facing: state.facing
})

export default connect(mapStateToProps)(Board)