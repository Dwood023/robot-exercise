import { 
    NORTH, WEST, SOUTH, EAST,
    PLACE, MOVE, LEFT, RIGHT,
    MAX_COORDINATES, BOARD_SIZE
} from './types';
import {initState} from './store'

export const valid_facing = (facing) => {
    console.log(`is facing ${facing} valid?`)
    switch (facing) {
        case NORTH:
            return true;
        case WEST:
            return true;
        case EAST:
            return true;
        case SOUTH:
            return true;
        default:
            return false;
    }
};
export const valid_position = (position) => (
    position.x <= MAX_COORDINATES.x &&
    position.y <= MAX_COORDINATES.y 
);


export const reducer = (state = initState, action) => {

    console.log("State:");
    console.log(state);
    console.log("Action:");
    console.log(action);

    switch (action.type) {
        case PLACE:
            if (valid_position(action.position) && valid_facing(action.facing)) {
                console.log("F")
                return {
                    position: action.position,
                    facing: action.facing
                };
            }
            else return state;

        case LEFT:
            switch (state.facing) {
                case NORTH:
                    return {...state, facing: WEST }
                case EAST:
                    return {...state, facing: NORTH }
                case SOUTH:
                    return {...state, facing: EAST }
                case WEST:
                    return {...state, facing: SOUTH }
                default:
                    return state
            }
        case RIGHT:
            switch (state.facing) {
                case NORTH:
                    return {...state, facing: EAST }
                case EAST:
                    return {...state, facing: SOUTH }
                case SOUTH:
                    return {...state, facing: WEST }
                case WEST:
                    return {...state, facing: NORTH }
                default:
                    return state

            }
        case MOVE:
            switch (state.facing) {
                case NORTH:
                    if ((state.position.y + 1) <= MAX_COORDINATES.y)
                        return {
                            ...state, 
                            position: {
                                x: state.position.x,
                                y: state.position.y + 1
                            }
                        }
                    else return state

                case EAST:
                    if ((state.position.x + 1) <= MAX_COORDINATES.x)
                        return {
                            ...state, 
                            position: {
                                x: state.position.x + 1,
                                y: state.position.y
                            }
                        }
                    else return state
                case SOUTH:
                    if ((state.position.y - 1) >= 0)
                        return {
                            ...state, 
                            position: {
                                x: state.position.x,
                                y: state.position.y - 1
                            }
                        }
                    else return state
                case WEST:
                    if ((state.position.x - 1) >= 0)
                        return {
                            ...state, 
                            position: {
                                x: state.position.x - 1,
                                y: state.position.y
                            }
                        }
                    else return state
            }
        default: 
            return state;
    }
}
