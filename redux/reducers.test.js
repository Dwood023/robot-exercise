import {reducer} from './reducers'
import * as types from './types'
import { start } from 'repl';

describe('Reducer handles board state', () => {

    const start_position = {
        facing: types.SOUTH,
        position: {
            x: 3,
            y: 2
        }
    }

    it("Can place robot", () => {
        let action = {
            type: types.PLACE,
            ...start_position
        };

        expect(reducer({}, action))
            .toEqual(start_position);

    })
    it("Can place robot then move", () => {
        let action = {
            type: types.PLACE,
            ...start_position,
            facing: types.NORTH
        };
        let state = reducer({}, action);

        expect(reducer(state, {type: types.MOVE}))
            .toEqual({facing: types.NORTH, position: {x: 3, y: 3}});

    })
    it("Can rotate robot left", () => {
        expect(
            reducer(start_position, {type: types.LEFT})
        ).toEqual({...start_position, facing: types.EAST})
        expect(
            reducer({...start_position, facing: types.EAST}, {type: types.LEFT})
        ).toEqual({...start_position, facing: types.NORTH})
    })
    it("Can move robot forward (up)", () => {
        expect(
            reducer({position: {x: 2, y: 2}, facing: types.NORTH}, {type: types.MOVE})
        ).toEqual({facing: types.NORTH, position: {x: 2, y: 3}})
    })
    it("Can move robot forward (down)", () => {
        expect(
            reducer(start_position, {type: types.MOVE})
        ).toEqual({...start_position, position: {x: 3, y: 1}})
    })
    it("Can move robot forward (left)", () => {
        expect(
            reducer({...start_position, facing: types.WEST}, {type: types.MOVE})
        ).toEqual({facing: types.WEST, position: {x: 2, y: 2}})
    })
    it("Can move robot forward (right)", () => {
        expect(
            reducer({position: {x: 3, y: 2}, facing: types.EAST}, {type: types.MOVE})
        ).toEqual({facing: types.EAST, position: {x: 4, y: 2}})
    })
    it("Can rotate robot right", () => {

        expect(
            reducer(start_position, {type: types.RIGHT})
        ).toEqual({...start_position, facing: types.WEST})
    })
    it("Prohibits robot self-termination", () => {
        let ready_to_jump = {
            position: {
                x: 2,
                y: 4
            },
            facing: types.NORTH
        };

        expect(
            reducer(
                ready_to_jump,
                { type: types.MOVE }
            )
        ).toEqual(ready_to_jump)
    })
});