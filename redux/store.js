import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {reducer} from './reducers'
import * as types from './types';

export const initState = {
    position: {
        x: 2,
        y: 2
    },
    facing: types.NORTH
};

export function initializeStore(initialState = initState) {
    return createStore(
        reducer,
        initialState,
        applyMiddleware(thunk)
    )
}