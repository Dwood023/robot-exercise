export const 
    PLACE = 'PLACE',
    MOVE = 'MOVE',
    LEFT = 'LEFT',
    RIGHT = 'RIGHT',
    REPORT = 'REPORT';

export const
    NORTH = 'NORTH',
    EAST = 'EAST',
    SOUTH = 'SOUTH',
    WEST = 'WEST';

export const BOARD_SIZE = {
    y: 5,
    x: 5
}
export const MAX_COORDINATES = {
    y: BOARD_SIZE.y - 1,
    x: BOARD_SIZE.x - 1
}